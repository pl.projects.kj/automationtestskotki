package com.kotki.automationTests;

import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.springframework.test.context.event.annotation.BeforeTestMethod;


import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.when;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.path.json.JsonPath.given;
import static org.hamcrest.Matchers.*;

public class FirstJUnit {
    @Test
    public void GetBooksDetails() {
        RestAssured.given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body("\"00")
                .when()
                .post("http://localhost:4000/registration")
                .then()
                .assertThat()
                .body(is("{}}"));
    }
}

