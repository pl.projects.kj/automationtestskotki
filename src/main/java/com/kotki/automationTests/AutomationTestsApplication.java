package com.kotki.automationTests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomationTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomationTestsApplication.class, args);
	}

}
